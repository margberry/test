### Resource Bundles and Multiple Languages

Due to the fact that we have all the text of label fields in one file, we can easily change it to another language, for example, _Spanish_. 

We need to create a file and call it [i18n_es.properties](webapp/i18n/i18n_es.properties). In this file we change the _English_ text to the _Spanish_ text.

If default language of your user's browser is Spanish, then the text of the labels will be on this language.

